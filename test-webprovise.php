<?php
class Travel
{
    public $id;
    public $createdAt;
    public $employeeName;
    public $departure;
    public $destination;
    public $price;
    public $companyId;

    function set_id($id) {
        $this->id = $id;
    }
    function get_id() {
        return $this->id;
    }
    function set_createdAt($createdAt) {
        $this->createdAt = $createdAt;
    }
    function get_createdAt() {
        return $this->createdAt;
    }
    function set_employeeName($employeeName) {
        $this->employeeName = $employeeName;
    }
    function get_employeeName() {
        return $this->employeeName;
    }
    function set_departure($departure) {
        $this->departure = $departure;
    }
    function get_departure() {
        return $this->departure;
    }
    function set_destination($destination) {
        $this->destination = $destination;
    }
    function get_destination() {
        return $this->destination;
    }
    function set_price($price) {
        $this->price = $price;
    }
    function get_price() {
        return $this->price;
    }
    function set_companyId($companyId) {
        $this->companyId = $companyId;
    }
    function get_companyId() {
        return $this->companyId;
    }
}
class Company
{
    public $id;
    public $name;
    public $parent;
    public $createdAt;
    public $travel;
    public $children;
    public $total_travel_cost;

    function set_id($id) {
        $this->id = $id;
    }
    function get_id() {
        return $this->id;
    }

    function set_name($name) {
        $this->name = $name;
    }
    function get_name() {
        return $this->name;
    }
    function set_parent($parent) {
        $this->parent = $parent;
    }
    function get_parent() {
        return $this->parent;
    }
    function set_createdAt($createdAt) {
        $this->createdAt = $createdAt;
    }
    function get_createdAt() {
        return $this->createdAt;
    }
    function set_travel($travel) {
        $this->travel = $travel;
    }
    function get_travel() {
        return $this->travel;
    }
    function set_children($children) {
        $this->children = $children;
    }
    function get_children() {
        return $this->children;
    }
    function set_total_travel_cost($total_travel_cost) {
        $this->total_travel_cost = $total_travel_cost;
    }
    function get_total_travel_cost() {
        return $this->total_travel_cost;
    }

}
class TestScript
{
    public function execute()
    {
        // travels
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://5f27781bf5d27e001612e057.mockapi.io/webprovise/travels',
            CURLOPT_SSL_VERIFYPEER => false
        ));
        $resp = curl_exec($curl);
        $travels = json_decode($resp);
        $arr_travel = [];
        foreach ($travels as $travel){
            $travel_data = new Travel();
            $travel_data->set_id($travel->id);
            $travel_data->set_createdAt($travel->createdAt);
            $travel_data->set_employeeName($travel->employeeName);
            $travel_data->set_departure($travel->departure);
            $travel_data->set_destination($travel->destination);
            $travel_data->set_price($travel->price);
            $travel_data->set_companyId($travel->companyId);
            array_push($arr_travel, $travel_data);
        }
        curl_close($curl);
        // companies
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://5f27781bf5d27e001612e057.mockapi.io/webprovise/companies',
            CURLOPT_SSL_VERIFYPEER => false
        ));
        $resp = curl_exec($curl);
        $companies = json_decode($resp);
        $arr_company = [];
        foreach ($companies as $company){
            $company_data = new Company();
            $company_data->set_id($company->id);
            $company_data->set_name($company->name);
            $company_data->set_parent($company->parentId);
            $company_data->set_createdAt($company->createdAt);
            array_push($arr_company, $company_data);
        }

        curl_close($curl);
        // calculate result
        $company_travel = [];
        foreach ($arr_company as $company){
            $tra = [];
            foreach ($arr_travel as $travel){
                if($travel->get_companyId() == $company->get_id()){
                    array_push($tra, $travel);
                }
            }
            $company->set_travel($tra);
            $travel_cost = 0;
            foreach($company->get_travel() as $tr){
                $travel_cost += $tr->get_price();
            }
            $company->set_total_travel_cost($travel_cost);
            $arr_child = [];
            $total_travel_cost = 0;
            foreach ($arr_company as $co){
                if($company->get_id() == $co->get_parent()){
                    array_push($arr_child, $co);
                    $total_travel_cost += $co->get_total_travel_cost();
                }
            }
            $company->set_children($arr_child);
            $company->get_total_travel_cost($total_travel_cost);

            array_push($company_travel, $company);
        }
        var_dump(json_encode($company_travel));
    }
}
(new TestScript())->execute();